using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHolder : MonoBehaviour
{
    private List<KeyItem.KeyItemType> keyItemList;
    private bool itemInRange;
    private bool itemIsPickedUp;
    private Collider itemObject;
    private KeyItem keyItem;

    /*[SerializeField] private Transform itemDest;*/
    [SerializeField] private GameObject Hand;


    private void Awake()
    {
        keyItemList = new List<KeyItem.KeyItemType>();
    }

    private void Update()
    {
        if (Input.GetKeyDown("e") && itemInRange && !itemIsPickedUp)
        {
            TakeItem();
        }

        else if (Input.GetKeyDown("e") && itemIsPickedUp)
        {
            DropItem();
        }
    }

    private void TakeItem()
    {
        itemIsPickedUp = true;
        itemObject.GetComponent<SphereCollider>().enabled = false;
        itemObject.GetComponent<CapsuleCollider>().enabled = false;
        itemObject.GetComponent<Rigidbody>().useGravity = false;
        itemObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        itemObject.transform.parent = Hand.transform;
        itemObject.transform.localPosition = keyItem.PickUpPosition;
        itemObject.transform.localEulerAngles = keyItem.PickUpRotation;
        /*itemObject.transform.position = itemDest.position;
        itemObject.transform.rotation = itemDest.rotation;
        itemObject.transform.parent = GameObject.Find("PlayerHand").transform;*/
        if (keyItem != null)
        {
            AddKeyItem(keyItem.GetKeyItemType());
        }
    }

    private void DropItem()
    {
        itemIsPickedUp = false;
        itemObject.transform.parent = null;
        /*itemObject.transform.parent = null;*/
        itemObject.GetComponent<SphereCollider>().enabled = true;
        itemObject.GetComponent<CapsuleCollider>().enabled = true;
        itemObject.GetComponent<Rigidbody>().useGravity = true;
        itemObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        if (keyItem != null)
        {
            RemoveKeyItem(keyItem.GetKeyItemType());
        }
    }

    public void AddKeyItem(KeyItem.KeyItemType keyItemType)
    {
        Debug.Log("Added keyItem" + keyItemType);
        keyItemList.Add(keyItemType);
    }

    public void RemoveKeyItem(KeyItem.KeyItemType keyItemType)
    {
        Debug.Log("Removed keyItem" + keyItemType);
        keyItemList.Remove(keyItemType);
    }

    public bool ContainsKeyItem(KeyItem.KeyItemType keyItemType)
    {
        return keyItemList.Contains(keyItemType);
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Item") && !itemIsPickedUp)
        {
            itemInRange = true;
            itemObject = collision;
            keyItem = itemObject.GetComponent<KeyItem>();
        }

        KeyDoor keyDoor = collision.GetComponent<KeyDoor>();
        if (keyDoor != null)
        {
           if (ContainsKeyItem(keyDoor.GetKeyItemType()))
            {
                RemoveKeyItem(keyDoor.GetKeyItemType());
                keyDoor.OpenDoor();
            }
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.CompareTag("Item") && !itemIsPickedUp)
        {
            itemInRange = false;
            itemObject = null;
            keyItem = null;
        }
    }


}


