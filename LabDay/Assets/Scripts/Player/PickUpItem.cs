using UnityEngine;
using System.Collections;

public class PickUpItem : MonoBehaviour
{
    private bool isInRange;
    public bool itemIsPickedUp = false;
    public Animator animator;

    public Transform theDest;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("e") && isInRange && !itemIsPickedUp)
        {
            animator.SetInteger("Actions", 1);
            StartCoroutine(waitEndOfAnimation());
        }

        else if (Input.GetKeyDown("e") && itemIsPickedUp)
        {
            animator.SetInteger("Actions", 1);
            DropItem(); 
        }
    }

    void TakeItem()
    {
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        this.transform.position = theDest.position;
        this.transform.parent = GameObject.Find("PlayerHand").transform;
        itemIsPickedUp = true;
    }

    void DropItem()
    {
        this.transform.parent = null;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<BoxCollider>().enabled = true;
        itemIsPickedUp = false;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            isInRange = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            isInRange = false;
        }
    }

    IEnumerator waitEndOfAnimation(){
        yield return new WaitForSeconds(4f);
        TakeItem();
        animator.SetInteger("Actions", 0);
    }
}