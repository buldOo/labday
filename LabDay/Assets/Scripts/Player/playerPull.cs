using UnityEngine;

public class playerPull : MonoBehaviour
{
    public GameObject playerHand;
    public PlayerMovement playerMovement;

    private void OnTriggerStay(Collider collision)
    {
        float ActualAngle = this.transform.eulerAngles.y;

        if(collision.gameObject.tag == "movable" && Input.GetKey("e"))
        {
            playerMovement.canRotate = false;
            Vector3 target = new Vector3(playerHand.transform.position.x, collision.gameObject.transform.position.y, playerHand.transform.position.z);
            collision.gameObject.transform.position = Vector3.MoveTowards(collision.gameObject.transform.position, target, Time.deltaTime * 5f);
        }
        else
        {
            playerMovement.canRotate = true;
        }
    }
}
