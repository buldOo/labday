using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController characterController;
    public Transform cam;
    public Animator animator;
    public PickUpItem pickUpItem;

    public float speed = 6f;
    Vector3 moveDirection;

    float horizontal;
    float vertical;

    public float turnSmoothTime = 0.1f;
    public bool canRotate = true;
    float turnSmoothVelocity;

    #region - Gravity variables -

    public float gravity;
    private float currentGravity;
    public float constantGravity;
    public float maxGravity;

    private Vector3 gravityDirection;
    private Vector3 gravityMovement;

    #endregion

    private void Start(){
    }

    private void Awake()
    {
        gravityDirection = Vector3.down;
        canRotate = true;
    }

    void Update()
    {
        CalculateGravity();

        #region - Movement -

        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if(direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            if (canRotate == true){ transform.rotation = Quaternion.Euler(0f, angle, 0f); }

            Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            if(/*pickUpItem.itemIsPickedUp == false*/ true){
                if (Input.GetKey(KeyCode.LeftShift) && canRotate == true)
                {
                    animator.SetInteger("Movement", 2);
                    characterController.Move(moveDirection.normalized * speed * 2 * Time.deltaTime + gravityMovement);
                }
                else
                {
                    animator.SetInteger("Movement", 1);
                    characterController.Move(moveDirection.normalized * speed * Time.deltaTime + gravityMovement);
                    
                }     
            }
            else{
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    animator.SetInteger("Movement", 5);
                    characterController.Move(moveDirection.normalized * speed * 2 * Time.deltaTime + gravityMovement);
                }
                else
                {
                    animator.SetInteger("Movement", 4);
                    characterController.Move(moveDirection.normalized * speed * Time.deltaTime + gravityMovement);                 
                }    
            }
        }
        else
        {
            if (pickUpItem.itemIsPickedUp == false)
            {
                animator.SetInteger("Movement", 0);
            }
            else
            {
                animator.SetInteger("Movement", 3);
            }
        }

        #endregion
    }

    #region - Gravity -

    private bool IsGrounded()
    {
        return characterController.isGrounded;
    }

    private void CalculateGravity()
    {
        if (IsGrounded())
        {
            currentGravity = constantGravity;
        }
        else
        {
            if(currentGravity > maxGravity)
            {
                currentGravity -= gravity * Time.deltaTime;
            }
        }

        gravityMovement = gravityDirection * -currentGravity;
    }

    #endregion
}
