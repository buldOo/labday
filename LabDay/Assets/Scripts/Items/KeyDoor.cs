using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : MonoBehaviour
{
    [SerializeField] private KeyItem.KeyItemType keyItemType;

    public KeyItem.KeyItemType GetKeyItemType()
    {
        return keyItemType;
    }

    public void OpenDoor()
    {
        gameObject.SetActive(false);
    }
}
