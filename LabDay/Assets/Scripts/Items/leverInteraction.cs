using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leverInteraction : MonoBehaviour
{
    public bool isLeverInRange = false;
    public bool isLeverActivate = false;

    void OnTriggerEnter(Collider collision){
        if(collision.CompareTag("Player")){
            isLeverInRange = true;
        }
    }

    void OnTriggerExit(Collider collision){
        if(collision.CompareTag("Player")){
            isLeverInRange = false;
        }
    }

    void Update(){
        if(isLeverInRange && Input.GetKeyDown("v")){
            if(isLeverActivate == true){
                // code to perform when activating the lever
                print(" ACTIF ");
                isLeverActivate = false;
            } else{
                // code to perform when activating the lever for the second time
                print(" NON ACTIF ");
                isLeverActivate = true;
            }
        }
    }    
}
