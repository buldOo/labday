using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItem : MonoBehaviour
{
    [SerializeField] private KeyItemType keyItemType;
    public Vector3 PickUpPosition;
    public Vector3 PickUpRotation;

    public enum KeyItemType
    {
        Key1,
        Key2,
        Key3
    }

    public KeyItemType GetKeyItemType()
    {
        return keyItemType;
    }
}
